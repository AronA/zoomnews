package com.idroidz.zoomnews.ui.home

import android.app.Activity
import com.nhaarman.mockito_kotlin.anyOrNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.lang.ref.WeakReference


@RunWith(MockitoJUnitRunner::class)
class NewsRouterTest {
    @Mock
    lateinit var activity: Activity

    private lateinit var newsRouter: NewsRouter

    @Before
    fun setUp() {
        newsRouter = NewsRouter(WeakReference(activity))
    }

    @Test
    fun `navigate shows image detail screen when route is image detail`() {
        newsRouter.navigate(NewsRouter.Route.NEWS_DETAIL)

        verify(activity).startActivity(anyOrNull())
    }
}