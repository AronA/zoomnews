package com.idroidz.zoomnews.ui.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idroidz.domain.model.NetworkCommentLike
import com.idroidz.domain.model.News
import com.idroidz.domain.usecase.GetNewsDetailUseCase
import com.idroidz.util.TestCoroutineRule
import com.idroidz.util.TestingCoroutineContextProvider
import com.idroidz.zoomnews.NewsApplication
import com.idroidz.zoomnews.util.CoroutineContextProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit

@RunWith(JUnit4::class)
class NewsDetailViewModelTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var newsDetailViewModel: NewsDetailViewModel
    private var contextProvider: CoroutineContextProvider = TestingCoroutineContextProvider()

    @Mock
    private lateinit var application: NewsApplication

    @Mock
    lateinit var getNewsDetailUseCase: GetNewsDetailUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        newsDetailViewModel = NewsDetailViewModel(application, getNewsDetailUseCase, contextProvider)
    }

    @Test
    fun `test getNewsDetails returns success response`() {
        testCoroutineRule.runBlockingTest {
            val news = News("test","test","","","","","")
            val result=GetNewsDetailUseCase.Result.Success(NetworkCommentLike("10","10"))
            given(getNewsDetailUseCase.getNewsDetail("")).willReturn(result)
            newsDetailViewModel.getNewsDetails(news)
            assertEquals(newsDetailViewModel.comments.get(),"10")

        }
    }

    @Test
    fun `test getNewsDetails returns failure response`() {
        testCoroutineRule.runBlockingTest {
            val news = News("test","test","","","","","")
            val result=GetNewsDetailUseCase.Result.Failure(Exception())
            given(getNewsDetailUseCase.getNewsDetail("")).willReturn(result)
            newsDetailViewModel.getNewsDetails(news)
            assertEquals(newsDetailViewModel.successResult.get(),false)

        }
    }
}