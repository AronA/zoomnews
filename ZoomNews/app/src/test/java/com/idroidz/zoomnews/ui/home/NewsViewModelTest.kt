package com.idroidz.zoomnews.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idroidz.domain.model.News
import com.idroidz.domain.usecase.GetNewsListUseCase
import com.idroidz.util.TestCoroutineRule
import com.idroidz.util.TestingCoroutineContextProvider
import com.idroidz.zoomnews.NewsApplication
import com.idroidz.zoomnews.util.CoroutineContextProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit

@RunWith(JUnit4::class)
class NewsViewModelTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var newsViewModel: NewsViewModel
    private var contextProvider: CoroutineContextProvider = TestingCoroutineContextProvider()

    @Mock
    private lateinit var application: NewsApplication

    @Mock
    lateinit var getNewsListUseCase: GetNewsListUseCase

    @Mock
    lateinit var newsRouter: NewsRouter


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        newsViewModel = NewsViewModel(application, getNewsListUseCase, newsRouter, contextProvider)
    }

    @Test
    fun test_getNews_returns_news_list() {
        testCoroutineRule.runBlockingTest {
            val newsList = mutableListOf<News>()
            val result = GetNewsListUseCase.Result.Success(newsList)
            given(getNewsListUseCase.getNewsList()).willReturn(result)

            newsViewModel.getNews()
            assertEquals(newsViewModel.newsList, newsList)

        }

    }

    @Test
    fun test_getNews_returns_error() {
        testCoroutineRule.runBlockingTest {
            val result = GetNewsListUseCase.Result.Failure(Exception())
            given(getNewsListUseCase.getNewsList()).willReturn(result)

            newsViewModel.getNews()
            assertEquals(newsViewModel.networkFailure.get(), true)

        }

    }


}