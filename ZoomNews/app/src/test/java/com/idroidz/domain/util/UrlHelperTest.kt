package com.idroidz.domain.util


import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.junit.Assert.*

@RunWith(JUnit4::class)
internal class UrlHelperTest {

    @Test
    fun test_refreshNewsList_success_case() {
        val testUrl = "https://www.bloomberg.com/tosv2.html?vid=&uuid=93feec80-92b7-11eb-b694-c1db110e96f1&url=L25ld3MvYXJ0aWNsZXMvMjAyMS0wNC0wMS91LXMtaW4tdGFsa3Mtd2l0aC1hdXN0cmFsaWEtb24tdGFpd2FuLXJlc3BvbnNlLWRpcGxvbWF0LXNheXM="
        val expectedUrl = "www.bloomberg.com-tosv2.html?vid=&uuid=93feec80-92b7-11eb-b694-c1db110e96f1&url=L25ld3MvYXJ0aWNsZXMvMjAyMS0wNC0wMS91LXMtaW4tdGFsa3Mtd2l0aC1hdXN0cmFsaWEtb24tdGFpd2FuLXJlc3BvbnNlLWRpcGxvbWF0LXNheXM="
        val actualUrl = UrlHelper.getNewsArticleUrl(testUrl)
        assertEquals(expectedUrl, actualUrl)
    }

}