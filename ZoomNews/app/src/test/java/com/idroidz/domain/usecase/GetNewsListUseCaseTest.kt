package com.idroidz.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idroidz.domain.model.NetworkNews
import com.idroidz.domain.model.News
import com.idroidz.domain.model.NewsResponse
import com.idroidz.domain.model.Source
import com.idroidz.domain.repository.NewsRepository
import com.idroidz.util.TestCoroutineRule
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import java.lang.NullPointerException

@RunWith(JUnit4::class)
class GetNewsListUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()


    @Mock
    private lateinit var newsRepository: NewsRepository
    private lateinit var getNewsListUseCase: GetNewsListUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getNewsListUseCase = GetNewsListUseCase(newsRepository)
    }

    @Test
    fun `get news list with success response`() {
        testCoroutineRule.runBlockingTest {
            val networkNewsList = mutableListOf<NetworkNews>()
            networkNewsList.add(NetworkNews("","","","","","", Source("","")))
            val newsList = mutableListOf<News>()
            newsList.add(News("","","","","","", ""))
            val expectedResult = GetNewsListUseCase.Result.Success(newsList)
            val newsResponse = NewsResponse(networkNewsList, "0", "success")
            given(newsRepository.getNewsList()).willReturn(newsResponse)
            val actualResult = getNewsListUseCase.getNewsList()
            assertEquals(expectedResult, actualResult)
        }
    }

    @Test
    fun `get news detail with error response`() {
        testCoroutineRule.runBlockingTest {
            val expectedResult = GetNewsListUseCase.Result.Success(null)
            given(newsRepository.getNewsList()).willReturn(null)
            val actualResult = getNewsListUseCase.getNewsList()
            assertEquals(expectedResult, actualResult)
        }
    }
}