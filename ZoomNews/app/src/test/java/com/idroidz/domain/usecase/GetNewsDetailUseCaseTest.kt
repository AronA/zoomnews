package com.idroidz.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idroidz.domain.model.NetworkCommentLike
import com.idroidz.domain.repository.NewsRepository
import com.idroidz.util.TestCoroutineRule
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit

@RunWith(JUnit4::class)
class GetNewsDetailUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()


    @Mock
    private lateinit var newsRepository: NewsRepository
    private lateinit var getNewsDetailUseCase: GetNewsDetailUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getNewsDetailUseCase = GetNewsDetailUseCase(newsRepository)
    }

    @Test
    fun `get news detail with success response`() {
        testCoroutineRule.runBlockingTest {
            val likesComment = NetworkCommentLike("10", "10")
            val expectedResult = GetNewsDetailUseCase.Result.Success(likesComment)
            given(newsRepository.getNewsLikeCount("")).willReturn(likesComment)
            given(newsRepository.getNewsCommentCount("")).willReturn(likesComment)
            val actualResult = getNewsDetailUseCase.getNewsDetail("")
            assertEquals(expectedResult, actualResult)
        }
    }

    @Test
    fun `get news detail with likes failure response`() {
        testCoroutineRule.runBlockingTest {
            val likesComment = NetworkCommentLike(null, "10")
            val expectedResult = GetNewsDetailUseCase.Result.Success(likesComment)
            given(newsRepository.getNewsLikeCount("")).willReturn(null)
            given(newsRepository.getNewsCommentCount("")).willReturn(likesComment)
            val actualResult = getNewsDetailUseCase.getNewsDetail("")
            assertEquals(expectedResult, actualResult)
        }
    }

    @Test
    fun `get news detail with comments failure response`() {
        testCoroutineRule.runBlockingTest {
            val likesComment = NetworkCommentLike("10", null)
            val expectedResult = GetNewsDetailUseCase.Result.Success(likesComment)
            given(newsRepository.getNewsLikeCount("")).willReturn(likesComment)
            given(newsRepository.getNewsCommentCount("")).willReturn(null)
            val actualResult = getNewsDetailUseCase.getNewsDetail("")
            assertEquals(expectedResult, actualResult)
        }
    }

}