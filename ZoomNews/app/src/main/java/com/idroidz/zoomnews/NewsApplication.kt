package com.idroidz.zoomnews

import android.app.Application
import com.idroidz.zoomnews.dagger.app.ApplicationComponent
import com.idroidz.zoomnews.dagger.app.ApplicationModule

import com.idroidz.zoomnews.dagger.app.DaggerApplicationComponent


class NewsApplication : Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        inject()
    }

    fun inject() {
        component = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }
}