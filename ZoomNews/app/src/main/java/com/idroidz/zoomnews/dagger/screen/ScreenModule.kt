package com.idroidz.zoomnews.dagger.screen

import com.idroidz.zoomnews.dagger.scope.PerScreen
import com.idroidz.zoomnews.ui.base.BaseActivity
import com.idroidz.zoomnews.ui.home.NewsRouter
import dagger.Module
import dagger.Provides
import java.lang.ref.WeakReference

@Module
class ScreenModule(private val activity: BaseActivity) {

    @PerScreen
    @Provides
    fun providesActivity(): BaseActivity {
        return activity
    }

    @PerScreen
    @Provides
    fun providesNewsRouter(): NewsRouter {
        return NewsRouter(WeakReference(activity))
    }
}
