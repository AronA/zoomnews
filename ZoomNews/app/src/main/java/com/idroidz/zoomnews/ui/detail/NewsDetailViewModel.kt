package com.idroidz.zoomnews.ui.detail


import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.idroidz.domain.model.News
import com.idroidz.domain.usecase.GetNewsDetailUseCase
import com.idroidz.domain.usecase.GetNewsDetailUseCase.Result.*
import com.idroidz.domain.util.UrlHelper
import com.idroidz.zoomnews.NewsApplication
import com.idroidz.zoomnews.ui.base.BaseViewModel
import com.idroidz.zoomnews.util.CoroutineContextProvider
import kotlinx.coroutines.launch
import javax.inject.Inject

class NewsDetailViewModel @Inject constructor(
        application: NewsApplication,
        private val getNewsDetailUseCase: GetNewsDetailUseCase,
        contextProvider: CoroutineContextProvider
) : BaseViewModel(application, contextProvider) {

    val progressVisible = ObservableBoolean(false)
    val successResult = ObservableBoolean(false)
    val title = ObservableField<String>()
    val likes = ObservableField<String>()
    val comments = ObservableField<String>()
    val description = ObservableField<String>()
    val thumbnail = ObservableField<String>()

    fun getNewsDetails(news: News) {
        progressVisible.set(true)
        viewModelScope.launch(getContextProvider().io) {
            val url = UrlHelper.getNewsArticleUrl(news.newsUrl)
            val result: GetNewsDetailUseCase.Result = getNewsDetailUseCase.getNewsDetail(url)
            progressVisible.set(result == Loading)
            when (result) {
                is Success -> {
                    comments.set(result.data.comments)
                    likes.set(result.data.likes)
                    successResult.set(true)
                    setData(news)
                }
                is Failure -> {
                    setData(news)
                    successResult.set(false)
                }
            }
        }
    }

    private fun setData(news: News) {
        title.set(news.title)
        description.set(news.description)
        thumbnail.set(news.thumbnail)
    }
}