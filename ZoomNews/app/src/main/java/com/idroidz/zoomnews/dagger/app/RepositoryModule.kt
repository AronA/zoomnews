package com.idroidz.zoomnews.dagger.app


import com.idroidz.data.ApiManager
import com.idroidz.data.repository.NewsRepositoryManager
import com.idroidz.domain.repository.NewsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/** Dagger module Class to provide instances for repository module.
 * @author Aron
 */
@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideNewsRepository(apiManager: ApiManager): NewsRepository {
        return NewsRepositoryManager(apiManager)
    }

}
