package com.idroidz.zoomnews.dagger.app

import com.idroidz.zoomnews.dagger.screen.ScreenComponent
import com.idroidz.zoomnews.dagger.screen.ScreenModule
import com.idroidz.zoomnews.NewsApplication
import dagger.Component
import javax.inject.Singleton

/** Dagger component to inject dependencies to defined components.
 * @author Aron
 */
@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class,ApiModule::class ])
interface ApplicationComponent {

    fun inject(activity: NewsApplication)

    fun plus(screenModule: ScreenModule): ScreenComponent

}