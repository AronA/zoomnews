package com.idroidz.zoomnews.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.idroidz.zoomnews.util.CoroutineContextProvider

/** Base viewmodel class which will be inherited by all viewmodels used in the app.
 * @author Aron
 */
abstract class BaseViewModel(application: Application, contextProvider: CoroutineContextProvider) : AndroidViewModel(application) {

    private val contextProvider: CoroutineContextProvider = contextProvider

    /**
     * Returns the coroutine context provider for the app.
     * @return     contextProvider
     */
    open fun getContextProvider(): CoroutineContextProvider {
        return contextProvider
    }

    override fun onCleared() {
        super.onCleared()
    }

}