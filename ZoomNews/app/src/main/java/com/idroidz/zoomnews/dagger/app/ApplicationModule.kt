package com.idroidz.zoomnews.dagger.app


import com.idroidz.zoomnews.NewsApplication
import com.idroidz.zoomnews.util.CoroutineContextProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/** Dagger module Class to provide instances for app module.
 * @author Aron
 */
@Module
class ApplicationModule(private val application: NewsApplication) {

    @Provides
    @Singleton
    fun provideApplication(): NewsApplication {
        return application
    }

    @Provides
    @Singleton
    fun provideContextProvider(): CoroutineContextProvider {
        return CoroutineContextProvider.ContextProvider()
    }
}