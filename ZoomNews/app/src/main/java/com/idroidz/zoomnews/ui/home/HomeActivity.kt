package com.idroidz.zoomnews.ui.home


import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.idroidz.zoomnews.R
import com.idroidz.zoomnews.databinding.ActivityHomeBinding
import com.idroidz.zoomnews.ui.base.BaseActivity
import com.idroidz.zoomnews.ui.home.adapter.NewsListAdapter
import javax.inject.Inject

class HomeActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var newsViewModel: NewsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val binding: ActivityHomeBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_home)

        screenComponent.inject(this)

        binding.viewModel = newsViewModel
        binding.swipeToRefresh.setOnRefreshListener(this)
        newsViewModel.getNews()
    }

    companion object {
        /**
         * bindList uses Databinding to initialize the recyclerView using an ObservableList from the NewsViewModel
         * this is referenced in activity_home.xml as 'app:adapter={@viewModel}'
         */
        @JvmStatic
        @BindingAdapter("adapter")
        fun bindList(recyclerView: RecyclerView, viewModel: NewsViewModel) {
            val adapter = NewsListAdapter(viewModel.newsList)
            adapter.onItemClickListener = { viewModel.onNewsItemClicked(it) }
            recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
            recyclerView.adapter = adapter
        }
    }

    /**
     * Overriden method called when swipe down for refresh
     */
    override fun onRefresh() {
        newsViewModel.getNews()
    }

}