package com.idroidz.zoomnews.dagger.screen

import com.idroidz.zoomnews.dagger.scope.PerScreen
import com.idroidz.zoomnews.ui.detail.NewsDetailActivity
import com.idroidz.zoomnews.ui.home.HomeActivity
import dagger.Subcomponent

@PerScreen
@Subcomponent(modules = [ScreenModule::class])
interface ScreenComponent {

  fun inject(homeActivity: HomeActivity)

  fun inject(newsDetailActivity: NewsDetailActivity)

}
