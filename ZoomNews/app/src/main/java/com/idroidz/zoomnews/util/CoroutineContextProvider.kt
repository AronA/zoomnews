package com.idroidz.zoomnews.util

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/** Interface to provide the Coroutine context to viewmodels .
 * @author Aron
 */
interface CoroutineContextProvider {
    val main: CoroutineContext
        get() = Dispatchers.Main
    val io: CoroutineContext
        get() = Dispatchers.IO

    class ContextProvider : CoroutineContextProvider
}