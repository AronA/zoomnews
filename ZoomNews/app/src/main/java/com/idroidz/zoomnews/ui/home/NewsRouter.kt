package com.idroidz.zoomnews.ui.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.idroidz.zoomnews.ui.detail.NewsDetailActivity
import java.lang.ref.WeakReference

/**
 * NewsRouter handles navigation for the HomeActivity
 */
class NewsRouter(private val activityRef: WeakReference<Activity>) {

    enum class Route {
        NEWS_DETAIL
    }

    fun navigate(route: Route, bundle:Bundle = Bundle()) {
        when (route) {
            Route.NEWS_DETAIL -> { showNextScreen(NewsDetailActivity::class.java, bundle) }
        }
    }

    private fun showNextScreen(clazz: Class<*>, bundle: Bundle) {
        activityRef.get()?.startActivity(Intent(activityRef.get(), clazz).putExtras(bundle))
    }
}
