package com.idroidz.zoomnews.ui.base

import androidx.appcompat.app.AppCompatActivity
import com.idroidz.zoomnews.NewsApplication
import com.idroidz.zoomnews.dagger.app.ApplicationComponent
import com.idroidz.zoomnews.dagger.screen.ScreenModule

/** Base activity class which will be inherited by all activities used in the app.
 * @author Aron
 */
abstract class BaseActivity : AppCompatActivity(){
    val screenComponent by lazy {
        getApplicationComponent().plus(ScreenModule(this))
    }

    private fun getApplicationComponent(): ApplicationComponent {
        return (application as NewsApplication).component
    }
}