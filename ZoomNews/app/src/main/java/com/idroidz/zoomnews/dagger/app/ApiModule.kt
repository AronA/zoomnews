package com.idroidz.zoomnews.dagger.app

import com.idroidz.data.ApiManager
import com.idroidz.data.RetrofitApiManager
import com.idroidz.data.RetrofitNewsDetailEndpoint
import com.idroidz.data.RetrofitNewsEndpoint
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import com.idroidz.zoomnews.BuildConfig

/** Dagger module Class to provide instances for api module.
 * @author Aron
 */
@Module
class ApiModule {

    companion object {
        private const val BASE_URL: String = "https://newsapi.org/v2/"
        private const val BASE_DETAIL_URL: String = "https://cn-news-info-api.herokuapp.com/"
    }

    @Provides
    @Singleton
    fun provideApiManager(apiManager: RetrofitApiManager): ApiManager {
        return apiManager
    }

    @Provides
    @Singleton
    fun provideRetrofitNewsEndpoint(): RetrofitNewsEndpoint {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .apply {
                    addInterceptor(
                            Interceptor { chain ->
                                val builder = chain.request().newBuilder()
                                builder.header("Authorization", BuildConfig.NEWS_API_KEY)
                                return@Interceptor chain.proceed(builder.build())
                            }
                    )
                }
                .build()
        val retrofit: Retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
        return retrofit.create(RetrofitNewsEndpoint::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofitNewsDetailEndpoint(): RetrofitNewsDetailEndpoint {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
        val retrofit: Retrofit = Retrofit.Builder().baseUrl(BASE_DETAIL_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
        return retrofit.create(RetrofitNewsDetailEndpoint::class.java)
    }
}