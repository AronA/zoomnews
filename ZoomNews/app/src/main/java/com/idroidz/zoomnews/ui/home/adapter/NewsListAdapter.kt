package com.idroidz.zoomnews.ui.home.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.idroidz.zoomnews.ui.home.adapter.NewsListAdapter.Holder
import com.idroidz.domain.model.News
import com.idroidz.zoomnews.R
import com.idroidz.zoomnews.databinding.NewsItemBinding

/**
 * NewsListAdapter is used to display data for each news in the list
 */
class NewsListAdapter(newsList: ObservableList<News>) :
    ObservableRecyclerViewAdapter<News, Holder>(newsList) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            NewsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(holder.itemView.context, getItem(position))
    }

    class Holder(
        private val binding: NewsItemBinding,
        private val onItemClickListener: ((item: Any) -> Unit)?
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(context: Context, news: News) {
            Glide.with(context).load(news.thumbnail).error(R.drawable.error_image).into(binding.newsThumbnail)
            binding.newsTitle.text = news.title
            binding.newsPublishedDate.text = news.publishedTime
            binding.newsSource.text = news.source
            binding.root.setOnClickListener { onItemClickListener?.invoke(news) }
        }
    }
}
