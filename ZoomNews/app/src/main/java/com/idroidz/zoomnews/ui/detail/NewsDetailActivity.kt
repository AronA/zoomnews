package com.idroidz.zoomnews.ui.detail

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.idroidz.domain.model.News
import com.idroidz.zoomnews.R
import com.idroidz.zoomnews.databinding.ActivityNewsDetailBinding
import com.idroidz.zoomnews.ui.base.BaseActivity
import javax.inject.Inject

class NewsDetailActivity : BaseActivity() {

    @Inject
    lateinit var newsDetailViewModel: NewsDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityNewsDetailBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_news_detail)

        screenComponent.inject(this)

        binding.viewModel = newsDetailViewModel
        val news = intent.getSerializableExtra(EXTRA_NEWS_DATA) as? News
        news?.let {
            newsDetailViewModel.getNewsDetails(news)
        }
        setupActionBar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupActionBar() {
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(android.R.color.transparent)))
        supportActionBar!!.setHomeAsUpIndicator(ContextCompat.getDrawable(applicationContext, R.drawable.custom_back_arrow))
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }


    companion object {
        const val EXTRA_NEWS_DATA = "news_data"
    }
}