package com.idroidz.zoomnews.ui.home

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.viewModelScope
import com.idroidz.domain.model.News
import com.idroidz.domain.usecase.GetNewsListUseCase
import com.idroidz.domain.usecase.GetNewsListUseCase.Result.*
import com.idroidz.zoomnews.NewsApplication
import com.idroidz.zoomnews.ui.base.BaseViewModel
import com.idroidz.zoomnews.ui.detail.NewsDetailActivity
import com.idroidz.zoomnews.util.CoroutineContextProvider
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class NewsViewModel @Inject constructor(
        application: NewsApplication,
        private val getNewsListUseCase: GetNewsListUseCase,
        private val newsRouter: NewsRouter,
        contextProvider: CoroutineContextProvider
) : BaseViewModel(application, contextProvider) {
    private val TAG = "NewsViewModel"

    val progressVisible = ObservableBoolean(false)
    val newsList = ObservableArrayList<News>()
    val networkFailure = ObservableBoolean(false)

    fun getNews() {
        progressVisible.set(true)
        viewModelScope.launch(getContextProvider().io) {
            showResult(getNewsListUseCase.getNewsList())
        }

    }

    private suspend fun showResult (result: GetNewsListUseCase.Result)  {
        withContext(getContextProvider().main) {
            progressVisible.set(result == Loading)
            when (result) {
                is Success -> {
                    result.newsList?.let {
                        networkFailure.set(false)
                        newsList.addAll(it)
                    }

                }
                is Failure -> {
                    networkFailure.set(true)
                }
                else -> Log.d(TAG,"invalid case")
            }
        }
    }

    // Shows news detail screen based on news item clicked
    fun onNewsItemClicked(news: Any) {
        newsRouter.navigate(NewsRouter.Route.NEWS_DETAIL, Bundle().apply {
            putSerializable(NewsDetailActivity.EXTRA_NEWS_DATA, (news as News))
        })
    }

}