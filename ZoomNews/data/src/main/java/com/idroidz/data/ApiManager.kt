package com.idroidz.data

import com.idroidz.domain.model.NetworkCommentLike
import com.idroidz.domain.model.NewsResponse

interface ApiManager {
    suspend fun getNewsList(): NewsResponse

    suspend fun getNewsLikeCount(newsUrl: String): NetworkCommentLike

    suspend fun getNewsCommentCount(newsUrl: String): NetworkCommentLike
}