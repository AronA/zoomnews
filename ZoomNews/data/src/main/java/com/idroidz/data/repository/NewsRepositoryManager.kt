package com.idroidz.data.repository

import android.util.Log
import com.idroidz.data.ApiManager
import com.idroidz.domain.model.NetworkCommentLike
import com.idroidz.domain.model.NewsResponse
import com.idroidz.domain.repository.NewsRepository
import javax.inject.Inject

class NewsRepositoryManager @Inject constructor(private val apiManager: ApiManager) : NewsRepository {
    private val TAG = "NewsRepositoryManager"

    override suspend fun getNewsList(): NewsResponse {
        return apiManager.getNewsList()
    }

    override suspend fun getNewsLikeCount(articleId: String): NetworkCommentLike {
        val result= apiManager.getNewsLikeCount(articleId)
        Log.d(TAG, "getNewsLikeCount: "+result)
        return result
    }

    override suspend fun getNewsCommentCount(articleId: String): NetworkCommentLike {
        val result = apiManager.getNewsCommentCount(articleId)
        Log.d(TAG, "getNewsCommentCount: "+result)
        return result
    }
}