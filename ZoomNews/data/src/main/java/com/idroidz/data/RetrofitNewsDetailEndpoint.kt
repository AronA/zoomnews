package com.idroidz.data

import com.idroidz.domain.model.NetworkCommentLike
import retrofit2.http.GET
import retrofit2.http.Path

interface RetrofitNewsDetailEndpoint {
    @GET("likes/{newsUrl}")
    suspend fun getNewsLikeCount(@Path("newsUrl") newsUrl: String): NetworkCommentLike

    @GET("comments/{newsUrl}")
    suspend fun getNewsCommentCount(@Path("newsUrl") newsUrl: String): NetworkCommentLike
}