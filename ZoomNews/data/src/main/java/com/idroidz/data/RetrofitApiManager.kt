package com.idroidz.data


import com.idroidz.domain.model.NetworkCommentLike
import com.idroidz.domain.model.NewsResponse
import javax.inject.Inject

class RetrofitApiManager @Inject constructor(private val newsEndpoint: RetrofitNewsEndpoint, private val newsDetailEndpoint: RetrofitNewsDetailEndpoint) : ApiManager {

    override suspend fun getNewsList(): NewsResponse {
        return newsEndpoint.getNewsList()
    }

    override suspend fun getNewsLikeCount(newsUrl: String): NetworkCommentLike {
       return newsDetailEndpoint.getNewsLikeCount(newsUrl)
    }

    override suspend fun getNewsCommentCount(newsUrl: String): NetworkCommentLike {
      return  newsDetailEndpoint.getNewsCommentCount(newsUrl)
    }

}
