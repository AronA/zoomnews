package com.idroidz.data


import com.idroidz.domain.model.NewsResponse
import retrofit2.http.GET

interface RetrofitNewsEndpoint {
    @GET("top-headlines?country=us")
    suspend fun getNewsList(): NewsResponse
}
