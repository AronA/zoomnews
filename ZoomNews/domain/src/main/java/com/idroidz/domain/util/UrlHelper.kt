package com.idroidz.domain.util

/**
 * Helper class for url construction and conversions
 */
object UrlHelper {
    fun getNewsArticleUrl(newsUrl: String): String {
        return newsUrl.replace("https://", "").replace("/", "-")
    }
}