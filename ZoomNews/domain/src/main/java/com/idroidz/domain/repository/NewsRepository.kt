package com.idroidz.domain.repository

import com.idroidz.domain.model.NetworkCommentLike
import com.idroidz.domain.model.NewsResponse


interface NewsRepository {

    suspend fun getNewsList(): NewsResponse?

    suspend fun getNewsLikeCount(articleId: String): NetworkCommentLike

    suspend fun getNewsCommentCount(articleId: String): NetworkCommentLike

}
