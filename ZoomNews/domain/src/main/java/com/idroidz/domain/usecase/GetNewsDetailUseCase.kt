package com.idroidz.domain.usecase

import com.idroidz.domain.model.NetworkCommentLike
import com.idroidz.domain.repository.NewsRepository
import javax.inject.Inject

/**
 * GetNewsDetailUseCase
 *
 * Used to retrieve the news detail  and return the values in a Result object
 * @param newsRepository repository to get the list from
 */
class GetNewsDetailUseCase @Inject constructor(private val newsRepository: NewsRepository) {
    sealed class Result {
        object Loading : Result()
        data class Success(val data: NetworkCommentLike) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    suspend fun getNewsDetail(newsUrl: String): Result {
        return try {
            Result.Success(NetworkCommentLike(newsRepository.getNewsLikeCount(newsUrl).likes,newsRepository.getNewsCommentCount(newsUrl)?.comments))
        } catch (ex: Exception) {
            Result.Failure(ex)
        }
    }
}