package com.idroidz.domain.usecase


import com.idroidz.domain.model.News
import com.idroidz.domain.repository.NewsRepository
import com.idroidz.domain.util.asDomainModel
import javax.inject.Inject

/**
 * GetNewsListUseCase
 *
 * Used to retrieve the list of news and return the values in a Result object
 * @param newsRepository repository to get the list from
 */
class GetNewsListUseCase @Inject constructor(private val newsRepository: NewsRepository) {
    sealed class Result {
        object Loading : Result()
        data class Success(val newsList: List<News>?) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    suspend fun getNewsList(): Result {
        return try {
            Result.Success(newsRepository.getNewsList()?.asDomainModel())
        } catch (ex: Exception) {
            Result.Failure(ex)
        }
    }
}
