package com.idroidz.domain.model

import java.io.Serializable

/** Domain model Class required by UI components .
 * @author Aron
 */
data class News(
        val title: String,
        val source: String?,
        val description: String?,
        val newsUrl: String,
        val publishedTime: String?,
        val content: String?,
        val thumbnail: String?
):Serializable