package com.idroidz.domain.util

import com.idroidz.domain.model.News
import com.idroidz.domain.model.NewsResponse


/**
 * Convert Network results to domain objects
 */
fun NewsResponse.asDomainModel(): List<News> {
    return newsList.map {
        News(
                title = it.title,
                source = it.source.name,
                newsUrl = it.url,
                description = it.description,
                publishedTime = it.publishedAt?.split("T")?.get(0),
                content = it.content,
                thumbnail = it.newsImage
        )
    }
}

