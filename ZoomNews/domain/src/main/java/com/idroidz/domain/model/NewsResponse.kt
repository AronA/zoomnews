package com.idroidz.domain.model

import com.google.gson.annotations.SerializedName


data class NewsResponse(
    @SerializedName("articles") val newsList: List<NetworkNews>,
    @SerializedName("totalResults") val totalResults: String,
    @SerializedName("status") val status: String
)