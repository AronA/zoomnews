package com.idroidz.domain.model

data class NetworkCommentLike(val likes: String?,
                              val comments: String?) {
}