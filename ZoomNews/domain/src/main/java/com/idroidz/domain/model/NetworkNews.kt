package com.idroidz.domain.model


import com.google.gson.annotations.SerializedName


data class NetworkNews(
        val title: String,
        val description: String?,
        val publishedAt: String?,
        val content: String?,
        val url: String,
        @SerializedName("urlToImage") val newsImage: String?,
        val source: Source
)