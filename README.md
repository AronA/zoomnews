# ZoomNews

Simple news application using "https://newsapi.org/v2/"
* App has been developed using "Clean architecture"
* Home Screen shows the list of Headlines
* Selecting any news will show the details and will fetch the "likes" and "comments" of the selected news using a different api endpoint

## HIGHLIGHTS

* Swipe to refresh is implemented in home screen for refreshing the news list
* Unit tests with 70% code coverage achieved

## IMPROVEMENTS

* Offline caching can be implemented using ROOM
* The new List Adapter can be used instead of RecyleView Adapter to improve performance
* Paging library can be used to improve fetching performance in case of large api data

## LIBRARIES USED

* Dagger2
* Kotlin Coroutines
* Retrofit
* Glide
* JUnit


# Steps to run the project

1. Clone the project from Gitlab
2. Import the project using Android Studio(File->New->Import Project)
3. This should suffice, you might get a pop-up for changing the Android sdk location, just click ok
4. Project will be built and ready to run
